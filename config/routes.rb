Rails.application.routes.draw do
  resources :users
  namespace :api do 
    resources :sensors, only: [] do
      resources :measurements, only: [:create]
    end
  end

  namespace :admin do
    root to: 'sensor_types#index'
    resources :sensor_types
    resources :sensors
  end
  get 'index', :to => 'main#index'
  get 'signup', :to => 'main#signup'
  post 'signup', :to => 'main#create' 
  root :to => "main#index"

  namespace :dashboard do 
    root to: 'plants#index'
    resources :plants do
      get :associate_sensors, on: :collection
    end
  end
end
