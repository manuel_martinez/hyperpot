class UserMailer < ActionMailer::Base
  default from: "notifications@plantaas-poisonivy.rhcloud.com"
  
  def welcome_email(user)
    @user = user
    mail( :to => @user.email,
    :subject => 'Thanks for signing up for our amazing app' ) 
  end
end
