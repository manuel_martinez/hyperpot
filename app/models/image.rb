class Image < ActiveRecord::Base
  belongs_to :sensor
  mount_uploader :file, ImagesUploader
end
