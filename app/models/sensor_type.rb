class SensorType < ActiveRecord::Base
  extend Enumerize

  enumerize :sensor_type, in: [:temperature, :soil_moisture, :water_level, :camera]

  validates :vendor, :sensor_type, :units, presence: true
  
end
