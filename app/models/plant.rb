class Plant < ActiveRecord::Base

  has_many :sensors
  mount_uploader :timelapse, TimelapseUploader

  def images
    if @images.nil?
      @images = Image.where(sensor_id: camera_ids)
    end
    @images
  end

  def camera_ids
    @camera_ids ||= sensors.cameras.pluck(:id)
  end

  def generate_timelapse
    image_list = QuickMagick::ImageList.new
    images = self.images.where('created_at BETWEEN ? AND ?', Time.now - 3.minutes, Time.now)

    if images.any?
      images.each do |image|
        image_list << QuickMagick::Image.read(image.file.path)
      end
      image_list.format = 'gif'
      tmp_path = Rails.root.join('tmp',"#{Time.now.to_i}.gif")
      image_list.save(tmp_path.to_s)
      
      File.open(tmp_path) do |file|
        update_attribute(:timelapse, file)
      end
      
      File.delete(tmp_path)
    end    
  end

  def chart_data(start_time, end_time)
    series = []
    labels = (start_time.to_i..end_time.to_i).step(1.minute)
    self.sensors.each do |sensor|
      if !sensor.sensor_type.sensor_type.camera?
        key = sensor.sensor_type_text
        values = sensor.measurements.where('created_at BETWEEN ? AND ?', start_time, end_time).order(:created_at).pluck(:value)
        Rails.logger.info "[INFO] values are: #{values.inspect}"
        series << {
          name: key,
          data: values
        }
      end
    end
    {
      labels: labels,
      series: series
    }
  end


end
