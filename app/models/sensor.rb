class Sensor < ActiveRecord::Base

  delegate :sensor_type_text, to: :sensor_type
  delegate :vendor, to: :sensor_type

  belongs_to :sensor_type
  belongs_to :plant

  has_many :images
  has_many :measurements

  scope :cameras, ->{ includes(:sensor_type).where(sensor_types: { sensor_type: :camera }) }
end
