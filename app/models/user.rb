class User < ActiveRecord::Base
  validates_confirmation_of :password
  validates_presence_of :email
  validates_presence_of :username
  validates_presence_of :password
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }

  before_save :encrypt_password
  has_many :plants
  
  def encrypt_password
    self.password_salt = BCrypt::Engine.generate_salt
    self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
  end
  
  def self.authenticate(username, password)
  
  end
  attr_accessor :password
  
end
