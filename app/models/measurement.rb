class Measurement < ActiveRecord::Base

  belongs_to :sensor

  validates :value, numericality: true 


end
