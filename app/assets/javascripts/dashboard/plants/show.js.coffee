$(document).ready ->
    options = 
        axisX:
            labelInterpolationFnc: (value) ->
                date = new Date(value * 1000)
                addZeroes(date.getHours()) + ':' + addZeroes(date.getMinutes())

    Chartist.Line('#plant-chart', gon.chart_data, options)

addZeroes = (number) ->
    ('0' + number).slice(-2)
