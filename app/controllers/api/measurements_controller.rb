class Api::MeasurementsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :json

  before_filter :set_sensor

  params_for :image, :file
  params_for :measurement, :value

  def create
    if @sensor.sensor_type.sensor_type.camera?
      @measurement = @sensor.images.build(image_params)  
    else
      @measurement = @sensor.measurements.build(measurement_params)  
    end    
    
    if @measurement.save
      render json: @measurement, status: :created
    else
      render json: @measurement.errors, status: :unprocessable_entity
    end
    
  end

  protected
    def set_sensor
      @sensor = Sensor.find(params[:sensor_id])
    end

end
