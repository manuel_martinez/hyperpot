class MainController < ApplicationController
  
  params_for :user, :username, :password, :password_confirmation, :email
  
  def index
    
  end
  
  def signup
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      UserMailer.welcome_email(@user).deliver
      render 'signup_success'
    else
      render 'signup'
    end
  end
    
end
