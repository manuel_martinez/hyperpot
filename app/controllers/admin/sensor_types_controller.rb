class Admin::SensorTypesController < Admin::BaseController

  params_for :sensor_type, :vendor, :units, :sensor_type

  def index
    @sensor_types = SensorType.page(params[:page])
  end

  def new
    @sensor_type = SensorType.new
  end

  def create
    @sensor_type = SensorType.new(sensor_type_params)

    if @sensor_type.save
      redirect_to admin_sensor_types_url, notice: 'El tipo de sensor se ha agregado'
    else
      render action: 'new'
    end
  end

  def edit
    @sensor_type = SensorType.find(params[:id])
  end

  def update
    @sensor_type = SensorType.find(params[:id])

    if @sensor_type.update_attributes(sensor_type_params)
      redirect_to admin_sensor_types_url, notice: 'El tipo de sensor se ha actualizado'
    else
      render action: 'edit'
    end
  end

  def destroy
    @sensor_type = SensorType.find(params[:id])
    @sensor_type.destroy
    redirect_to admin_sensor_types_url, notice: 'El tipo de sensor se ha eliminado'
  end
end
