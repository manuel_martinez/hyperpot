class Admin::SensorsController < Admin::BaseController

  params_for :sensor, :sensor_type_id, :serial_number

  before_filter :set_sensor_types, only: [:new, :edit]

  def index
    @sensors = Sensor.page(params[:page])
  end

  def new
    @sensor = Sensor.new
    Rails.logger.info "[INFO] #{@sensor_types.inspect}"
  end

  def create
    @sensor = Sensor.new(sensor_params)

    if @sensor.save
      redirect_to admin_sensors_url, notice: 'El sensor se ha agregado'
    else
      set_sensor_types
      render action: 'new'
    end
  end

  def edit
    @sensor = Sensor.find(params[:id])
  end

  def update
    @sensor = Sensor.find(params[:id])

    if @sensor.update_attributes(sensor_params)
      redirect_to admin_sensors_url, notice: 'El sensor se ha actualizado'
    else
      set_sensor_types
      render action: 'edit'
    end
  end

  def destroy
    @sensor = Sensor.find(params[:id])
    @sensor.destroy
    redirect_to admin_sensors_url, notice: 'El sensor se ha eliminado'
  end

  protected
    def set_sensor_types
      @sensor_types = SensorType.order(:sensor_type)
    end
end
