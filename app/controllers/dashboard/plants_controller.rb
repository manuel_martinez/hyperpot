class Dashboard::PlantsController < Dashboard::BaseController

  def index
    @plants = Plant.all
  end

  def show
    @plant = Plant.find(params[:id])
    @plant.generate_timelapse
    start_time = Time.now - 30.minutes
    end_time = Time.now
    @images = @plant.images.where('created_at BETWEEN ? AND ?', start_time, end_time).order(created_at: :desc)
    gon.chart_data = @plant.chart_data(start_time, end_time)
  end

  def associate_sensors
    @plant = Plant.find_or_create_by(name: 'Alba')
    Sensor.update_all(plant_id: @plant.id)
    redirect_to 'https://www.youtube.com/watch?v=rLw-9dpHtcU'
  end
end
