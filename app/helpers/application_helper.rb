module ApplicationHelper
  def pagination(collection, options={})
    will_paginate collection, options.merge(renderer: BootstrapPagination::Rails)
  end

end
