# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140928163728) do

  create_table "images", force: true do |t|
    t.string   "file",       null: false
    t.integer  "sensor_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "measurements", force: true do |t|
    t.float    "value",      null: false
    t.integer  "sensor_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "plants", force: true do |t|
    t.string   "name",       limit: 100, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "timelapse"
  end

  create_table "sensor_types", force: true do |t|
    t.string   "vendor",                  null: false
    t.string   "sensor_type", limit: 100, null: false
    t.string   "units",       limit: 10,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sensors", force: true do |t|
    t.string   "serial_number",  limit: 100, null: false
    t.integer  "sensor_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "plant_id"
  end

  create_table "users", force: true do |t|
    t.string   "username",      null: false
    t.string   "email",         null: false
    t.string   "password_hash", null: false
    t.string   "password_salt", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
