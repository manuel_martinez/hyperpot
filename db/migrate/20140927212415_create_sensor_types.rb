class CreateSensorTypes < ActiveRecord::Migration
  def change
    create_table :sensor_types do |t|
      t.string :vendor, null: false
      t.string :type, limit: 100, null: false
      t.string :units, limit: 10, null: false
      t.timestamps
    end
  end
end
