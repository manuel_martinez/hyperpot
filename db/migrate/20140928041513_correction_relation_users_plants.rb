class CorrectionRelationUsersPlants < ActiveRecord::Migration
  def change
      drop_table :users
      create_table :users do |t|
      t.string :username,null:false
      t.string :email, null: false
      t.string :password_hash, null: false
      t.string :password_salt, null: false
      t.timestamps
      end

    change_table :plants do |t|
      t.references :user
      t.foreign_key :users
    end
  end
end
