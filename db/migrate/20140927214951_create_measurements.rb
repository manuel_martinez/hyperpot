class CreateMeasurements < ActiveRecord::Migration
  def change
    create_table :measurements do |t|
      t.float :value, null: false
      t.references :sensor
      t.foreign_key :sensors
      t.timestamps
    end
  end
end
