class AddTimelapseToPlants < ActiveRecord::Migration
  def change
    change_table :plants do |t|
      t.string :timelapse
    end
  end
end
