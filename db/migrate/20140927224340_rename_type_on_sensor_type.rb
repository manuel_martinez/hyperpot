class RenameTypeOnSensorType < ActiveRecord::Migration
  def change
    change_table :sensor_types do |t|
      t.rename :type, :sensor_type
    end
  end
end
