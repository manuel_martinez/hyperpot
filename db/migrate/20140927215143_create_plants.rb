class CreatePlants < ActiveRecord::Migration
  def change
    create_table :plants do |t|
      t.string :name, null: false, limit: 100
      t.references :user
      t.foreign_key :users
      t.timestamps
    end
  end
end
