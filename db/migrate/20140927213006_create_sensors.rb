class CreateSensors < ActiveRecord::Migration
  def change
    create_table :sensors do |t|
      t.string :serial_number, limit: 100, null: false
      t.references :sensor_type
      t.foreign_key :sensor_types, dependent: :delete
      t.timestamps
    end
  end
end
