class AddPlantIdToSensors < ActiveRecord::Migration
  def change
    change_table :sensors do |t|
      t.references :plant
      t.foreign_key :plants
    end
  end
end
