class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :file, null: false
      t.references :sensor
      t.foreign_key :sensors
      t.timestamps
    end
  end
end
